include /usr/share/selinux/devel/Makefile

define assign-nzbget-ports
echo "Assigning nzbget port..."
semanage port -m -t nzbget_port_t -p tcp 6789
semanage port -m -t nzbget_port_t -p tcp 6791
semanage port -m -t innd_port_t -p tcp 563
endef

define remove-nzbget-ports
echo "Removing nzbget port..."
semanage port -d -t nzbget_port_t -p tcp 6789
semanage port -d -t nzbget_port_t -p tcp 6791
semanage port -d -t innd_port_t -p tcp 563
endef

define relabel-nzbget-files
echo "Relabeling files..."
restorecon -DR /var/lib/nzbget
restorecon -DR /var/spool/nzbget
restorecon -DR /opt/nzbget
endef

.PHONY: install uninstall

install:
	semodule -v -i nzbget.pp
	$(assign-nzbget-ports)
	$(relabel-nzbget-files)

uninstall:
	semodule -v -r nzbget
	$(remove-nzbget-ports)
	$(relabel-nzbget-files)	

